# Carte

## Description

Cette application permet de lire une carte de niveau à partir d'une base, et de lui combiner plusieurs bases de données (eau, territoire...)


## Variables d'environnement

- CARTE_DEM_PATH : le chemin vers la base de donnée DEM
