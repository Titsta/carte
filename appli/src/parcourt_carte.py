#! /bin/python3.10
#-*- coding: utf-8 -*-

from matplotlib.widgets import Button
import numpy as np 				# python3 -m pip install numpy
import matplotlib.pyplot as plt # python3 -m pip install matplotlib
import matplotlib.colors

# TODO : Les bénéfices de mon cache ne sont pas flagrant à l'usage. Peut être le supprimer, car la mémoire doit être pompé par le cache.
# D'autant plus que pour l'instant, je ne le vide jamais.


# -------------------------- Colorbar -------------------------- #

class FixPointNormalize(matplotlib.colors.Normalize):
    """
    Inspired by https://stackoverflow.com/questions/20144529/shifted-colorbar-matplotlib
    Subclassing Normalize to obtain a colormap with a fixpoint
    somewhere in the middle of the colormap.

    This may be useful for a `terrain` map, to set the "sea level"
    to a color in the blue/turquise range.
    """
    def __init__(self, vmin=None, vmax=None, sealevel=0, col_val = 0.21875, clip=False):
        # sealevel is the fix point of the colormap (in data units)
        self.sealevel = sealevel
        # col_val is the color value in the range [0,1] that should represent the sealevel.
        self.col_val = col_val
        matplotlib.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.sealevel, self.vmax], [0, self.col_val, 1]
        return np.ma.masked_array(np.interp(value, x, y))


class ParcourtCarte:
    """
        Classe de Vue : visualise une liste de Factures par Matplotlib.
        Utilise deux boutons Précédent et Suivant

        Initialisé avec une liste de Facture.
    """

    def __init__(self, fig, ax, get_map, max_level, see_level, lat, long, debug=False):

        self.get_map= get_map
        self.coo    = (lat, long)
        self.cache  = {}

        self.fig    = fig
        self.ax     = ax
        self.max_level = max_level
        self.see_level = see_level
        self._init_colormap()

        fig.subplots_adjust(bottom=0.2)
        self.bt_nord = Button(fig.add_axes([0.45, 0.90, 0.1, 0.075]), 'Nord')
        self.bt_sud  = Button(fig.add_axes([0.45, 0.05, 0.1, 0.075]), 'Sud')
        self.bt_est  = Button(fig.add_axes([0.85, 0.45, 0.1, 0.075]), 'Est')
        self.bt_ouest= Button(fig.add_axes([0.05, 0.45, 0.1, 0.075]), 'Ouest')

        self.bt_nord.on_clicked(self.nord)
        self.bt_sud.on_clicked(self.sud)
        self.bt_est.on_clicked(self.est)
        self.bt_ouest.on_clicked(self.ouest)

        if debug :
            print(self.__dict__)

        fig.canvas.draw()

    def _init_colormap(self, max_level=None, see_level=None):
        # Combine the lower and upper range of the terrain colormap with a gap in the middle to let the coastline appear more prominently.
        # inspired by https://stackoverflow.com/questions/31051488/combining-two-matplotlib-colormaps

        if max_level == None : max_level = self.max_level
        if see_level == None : see_level = self.see_level

        colors_undersea = plt.cm.terrain(np.linspace(0, 0.17, 56))
        colors_land = plt.cm.terrain(np.linspace(0.25, 1, 200))
        # combine them and build a new colormap
        colors = np.vstack((colors_undersea, colors_land))

        self.cmap = matplotlib.colors.LinearSegmentedColormap.from_list('cut_terrain', colors)
        #self.cmap = 'terrain'
        self.norm = FixPointNormalize(sealevel=see_level, vmax=max_level, vmin=-100)


    def mem(self, coo):
        if not coo in self.cache :
            self.cache[coo] = self.get_map(*coo)
        return self.cache[coo]

    def draw(self):
        img = self.mem(self.coo)
        self.ax.imshow( img, cmap=self.cmap, norm=self.norm )
        #self.ax.draw(self.fig.canvas.renderer)
        self.fig.canvas.draw() # FIXME vérifier si c'est pas overkill
        #self.ax.colorbar()

    def nord(self,event):
        lat,long = self.coo
        if lat < 83 :
            lat += 1
            self.coo = (lat, long)
            self.draw()

    def sud(self, event):
        lat,long = self.coo
        if lat > -80 :
            lat -= 1
            self.coo = (lat, long)
            self.draw()

    def est(self,event):
        lat,long = self.coo
        if long < 179 :
            long += 1
        else :
            long = -180

        self.coo = (lat, long)
        self.draw()

    def ouest(self, event):
        lat,long = self.coo
        if long > -180 :
            long -= 1
        else :
            long = 179

        self.coo = (lat, long)
        self.draw()
