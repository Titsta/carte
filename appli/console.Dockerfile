FROM python:3.11-slim
RUN apt-get update && apt-get install -y python-tk && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip
ADD requirements.txt .
RUN pip install -r requirements.txt

ADD run.py .
ADD src/ src/
RUN mkdir /data

ENV CARTE_DEM=/data \
    X=50 \
    Y=-2 \
    MPLBACKEND=TKAgg

CMD python3 run.py
