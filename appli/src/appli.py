#! /bin/python3.10
#-*- coding: utf-8 -*-

import sys
from pathlib import Path, PurePath
import numpy as np 				# python3 -m pip install numpy
import matplotlib
import matplotlib.pyplot as plt # python3 -m pip install matplotlib

from src import arguments
from .parcourt_carte import ParcourtCarte

"""
Programme permettant d'afficher une carte.

## Spécification des fichiers sources

- Total size : 405 GB
- Data splitting : 22,551 files of 1° latitude x 1° longitude.
- File name : [latitude]_[longitude] of the lower left corner.
- File rows : 3,600
- File columns :
  - 3,600 below 50° latitude;
  - 2,400 between 50° and 60° latitude;
  - 1,800 between 60° and 70° latitude;
  - 1,200 between 70° and 80° latitude;
  - 720 above 80° latitude.
- Data type : 16-bit signed short integer (little endian byte order).
- Data unit : meter
- No data value: -9999
"""

#matplotlib.use('GTK3Agg') # Utilise l'interface graphique
# ['GTK3Agg', 'GTK3Cairo', 'GTK4Agg', 'GTK4Cairo', 'MacOSX', 'nbAgg', 'QtAgg', 'QtCairo', 'Qt5Agg', 'Qt5Cairo', 'TkAgg', 'TkCairo', 'WebAgg', 'WX', 'WXAgg', 'WXCairo', 'agg', 'cairo', 'pdf', 'pgf', 'ps', 'svg', 'template']
SEE_LEVEL   = arguments.see_level     # level of see
MAX_LEVEL   = arguments.max_level     # for white

FILE_ROW    = 3600
FILE_COL    = [3600,3600,3600,3600,3600,2400,1800,1200,720,720]
def get_size(latitude):
    return FILE_ROW, FILE_COL[abs(latitude)//10]


# ---------------------------------------------------- #

class Appli:
    """ Classe de l'application principale.
    Initialise une application à l'instanciation.
    Méthode run pour la lancer.
    """
    arguments   = None
    debug       = None
    path        = None

    def __init__(self, arguments):
        """ Constructeur de l'application """

        debug       = arguments.debug
        dem         = arguments.dem
        lat         = arguments.x
        long        = arguments.y
        max_level   = arguments.max_level
        see_level   = arguments.see_level

        if debug :
            print("Initialisation de l'application")
            print(arguments)

        # path
        if not dem :
            path = Path('..')
        elif PurePath(dem).is_absolute() :
            path = Path(dem)
        else :
            path = Path('..').joinpath(dem)
        path = path.resolve()
        assert path.exists(), "La base DEM n'existe pas"
        assert path.is_dir(), "Répertoire DEM incorrect"

        if debug :
            print('Répertoire DEM : ', path)

        # ---------- ParcourtCarte ---------- #

        fig, ax = plt.subplots()
        parcourt = ParcourtCarte(
            fig         = fig,
            ax          = ax,
            get_map     = lambda lat,long : self.get_map(lat, long),
            max_level   = max_level,
            see_level   = see_level,
            lat         = lat,
            long        = long,
            debug       = debug
        )

        # ---------- attribut initialisation ---------- #

        self.arguments  = arguments
        self.debug      = debug
        self.path       = path
        self.parcourt   = parcourt
        self.map_empty_cache = {}

    def get_map(self, lat, long):
        file = self.path/f'{lat}_{long}'
        size = get_size(lat)

        if file.is_file():
            if self.debug : print(f'get file {file}')
            map = np.fromfile(file, dtype=np.int16).reshape(size)
        else :
            if self.debug : print(f'No File {lat}_{long}')
            if not size in self.map_empty_cache :
                self.map_empty_cache[size] = np.empty(size, dtype=np.int16)
            map = self.map_empty_cache[size]
        return map

    def run(self):
        self.parcourt.draw()
        plt.show()
