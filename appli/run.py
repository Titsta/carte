#! /bin/python3.10
#-*- coding: utf-8 -*-

doc="""
Code lançant l'application.
"""

import unittest
from src import app, arguments

if __name__ == '__main__':
	if arguments.debug :
		print('-------- Unittest --------')
		# recherche les testes unitaires dans src/test et les executes avec le repertoir src comme base.
		all_tests = unittest.defaultTestLoader.discover('./src/test', top_level_dir='./src')
		unittest.TextTestRunner(verbosity=arguments.debug_detail).run(all_tests)

	print("-------- Lancement de l'application --------")
	app.run()
