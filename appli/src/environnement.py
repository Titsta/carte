#! /bin/python3
#-*- coding: utf-8 -*-

from os import getenv
from os.path import isfile

doc="""
Code permettant la création d'un objet "arguments", initialisé avec des valeurs par défaut ou les variables d'environnements.
Recherche et privilégie les éventuels secret docker correspondant aux variables. ( variable d'environnement avec _FILE )

À défaut de secret docker correspondant, utilise la variable d'environnement, à défaut utilise la valeur par défaut.

Les variables correspondantes sont formatées, en particulier les valeurs booléennes écrite en toute lettre. ( par exemple 'yes' ou 'True' )

"""


# ---------- Variables d'environnement nécessaires à l'application. ---------- #
# Définie également les valeurs par défaut, le nom de l'attribut où elle sera placée dans l'objet arguments et le formattage à utiliser.

DEFAULTS = (
#		ENV name			default value		name			parse
	('CARTE_DEBUG', 		True,				'debug',		bool 	),
	('CARTE_DEBUG_DETAIL',	2, 					'debug_detail',	int 	),
	('CARTE_DEM',			'raw/',				'dem', 			str 	),
	('CARTE_SEE_LEVEL',		100,				'see_level', 	int 	),
	('CARTE_MAX_LEVEL', 	1000, 				'max_level', 	int 	),
	('X',					50,					'x',			int 	),
	('Y',					-2,					'y',			int 	),
)

# ---------- Classe et format ---------- #

# Tupple utilisé pour formater les booléens. ( toute autre valeur est considérée comme False )
TRUE_VALUE = ('true','y','yes','1','on','oui','o')

class AttrDict(dict):
	"""Crée une classe dictionnaire dont les éléments sont accessibles comme des attributs"""
	def __init__(self, *args, **kwargs):
		super(AttrDict, self).__init__(*args, **kwargs)
		self.__dict__ = self

# Crée l'objet arguments, qui servira dans le code.
arguments = AttrDict()



# -------------------- Extraction des variables d'environment ---------------------- #

for key, value, name, parse in DEFAULTS :
	# value contient la valeur par défaut
	# file le chemin du fichier où se trouve la variable si le secret docker existe
	# env la valeur de la variable d'environnement récupérée.

	env = None
	file = getenv('{}_FILE'.format(key))	# Vérifie si une variable correspondante en _FILE existe, pour traitement des secrets docker

	if file and isfile(file) :				# si cette variable et que le fichier du secret docker existe, récupère sa valeur
		with open( file, "r" ) as f :
			env = f.read().rstrip('\n')
	else :
		env = getenv(key)					# S'il n'existe pas, récupère la variable d'environnement de base (sans le _FILE)

	# Formattage des variables d'environnement, si elle existe.
	# value reste inchangé si elle n'existe pas.
	if env != None :
		if parse == bool :						# les booléens nécessite un formatage un peu particulier.
			value = (env.lower() in TRUE_VALUE)
		else :
			value = env if parse == str else parse(env)		# pour les autres, on formate directement avec la classe

	arguments[name] = value	# Enregistre la valeur dans le dictionnaire "arguments"
